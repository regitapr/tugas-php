<?php
  if (isset($_POST['submit'])) {
    $nilaiuts = $_POST["uts"]* 0.35;
    $nilaiuas = $_POST["uas"]* 0.50 ;
    $nilaitugas = $_POST["tugas"]* 0.15;

    $nilai_total = $nilaiuts +$nilaiuas +$nilaitugas;
    if ($nilai_total <= 100) {
          $grade = "A";
      }
    elseif ($nilai_total < 90) {
        $grade = "B";
      }
    elseif ($nilai_total <= 70) {
        $grade = "C";
      }
    elseif ($nilai_total <= 50) {
        $grade = "D";
      }
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Hitung Nilai Akhir</title>
    <style>
      body {background-color: plum;}
    </style>
  </head>
  <body>
    <h1 class="text-center mt-3"><b>Menghitung Nilai Akhir</b></h1>
    <hr>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-7 border border-dark m-3 p-3">
          <form action="" method="POST">
              <div class="mb-3">
                <label for="nama" class="form-label">Nama</label>
                <input type="text" name="nama" class="form-control" id="nama">
              </div>
              <div class="mb-3">
                <label for="mapel" class="form-label">Mata Pelajaran</label>
                <input type="text" name="mapel" class="form-control" id="mapel">
              </div>
              <div class="mb-3">
                <label for="uts" class="form-label">Nilai UTS</label>
                <input type="number" name="uts" class="form-control" min="0" max="100" id="uts">
              </div>
              <div class="mb-3">
                <label for="uas" class="form-label">Nilai UAS</label>
                <input type="number" name="uas" class="form-control" min="0" max="100" id="uas">
              </div>
              <div class="mb-3">
                <label for="tugas" class="form-label">Nilai Tugas</label>
                <input type="number" name="tugas" class="form-control" min="0" max="100" id="tugas">
              </div>
              <button type="submit" name="submit" class="btn btn-dark">Submit</button>
            </form>
        </div>
      </div>
      <?php if(isset($_POST['submit'])): ?>
      <section>
        <h1 class="text-center"><b> Nilai Akhir</b></h1>
            <div class="container">
              <div class="row justify-content-center">
              <div class="col-7 border border-dark m-3 p-3">
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Nama</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $_POST['nama']?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Mata Pelajaran</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $_POST['mapel']?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Nilai UTS</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $_POST['uts']?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Nilai UAS</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $_POST['uas']?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Total Nilai</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $nilai_total?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Grade Nilai</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $grade?></p>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <section>
      <?php endif; ?>
    </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>